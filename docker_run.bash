#!/bin/bash
JEKYLL_VERSION=3.8
docker run --rm \
  -p 4000:4000 \
  --mount "source=$PWD,target=/srv/jekyll,type=bind" \
  -it jekyll/jekyll:$JEKYLL_VERSION \
  /bin/bash -c 'apk add python && apk add build-base tzdata && bundle && /bin/bash'

# then run
# bundle exec jekyll serve -w --incremental